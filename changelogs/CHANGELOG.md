
## 0.1.2 [02-28-2023]

* Bug fixes and performance improvements

See commit 4384641

---

## 0.1.1 [02-17-2023]

* Bug fixes and performance improvements

See commit e9f3b89

---
