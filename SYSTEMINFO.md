# Pitney Bowes

Vendor: Pitney Bowes
Homepage: https://www.pitneybowes.com/us

Product: Pitney Bowes
Product Page: https://www.pitneybowes.com/us

## Introduction
We classify Pitney Bowes into the Inventory domain because it provides the ability to manage inventory, shipment and tracking.

"Pitney Bowes makes smarter shipping decisions. Shop carrier rates to save on every parcel." 

## Why Integrate
The Pitney Bowes adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Pitney Bowes. With this adapter you have the ability to perform operations such as:

- Create, Reprint, Retry, and Void a Shipment.
- Create, Reprint and Retry Manifest.
- Validate and Suggest Addresses.
- Get transaction or archived reports.
- Tracking

## Additional Product Documentation
The [API documents for Pitney Bowes](https://docs.shippingapi.pitneybowes.com/overview.html)

