/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-pitney_bowes',
      type: 'PitneyBowes',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const PitneyBowes = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Pitney_bowes Adapter Test', () => {
  describe('PitneyBowes Class Tests', () => {
    const a = new PitneyBowes(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#oauthToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.oauthToken(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'oauthToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const carrierInfoGetCarrierFacilitiesBodyParam = {
      address: {
        addressLines: [
          '6525 Greenway Dr'
        ],
        cityTown: 'Roanoke',
        stateProvince: 'VA',
        countryCode: 'US'
      },
      carrier: 'USPS',
      carrierFacilityOptions: [
        {
          name: 'FACILITY_TYPE_SERVICE',
          value: 'LABEL_BROKER_RETAIL'
        },
        {
          name: 'FACILITY_TYPE',
          value: 'POST_OFFICE'
        },
        {
          name: 'FACILITY_WITHIN_RADIUS',
          value: '10'
        },
        {
          name: 'NUMBER_OF_FACILITIES',
          value: '5'
        }
      ]
    };
    describe('#getCarrierFacilities - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCarrierFacilities(carrierInfoGetCarrierFacilitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CarrierInfo', 'getCarrierFacilities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const carrierInfoCarrier = 'fakedata';
    const carrierInfoOriginCountryCode = 'fakedata';
    describe('#getCarrierLicenseAgreement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCarrierLicenseAgreement(carrierInfoCarrier, carrierInfoOriginCountryCode, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('<text>', data.response.licenseText);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CarrierInfo', 'getCarrierLicenseAgreement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCarrierSupportedDestination - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCarrierSupportedDestination(carrierInfoCarrier, carrierInfoOriginCountryCode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CarrierInfo', 'getCarrierSupportedDestination', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const carrierInfoDestinationCountryCode = 'fakedata';
    describe('#getCarrierServiceRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCarrierServiceRules(carrierInfoCarrier, carrierInfoOriginCountryCode, carrierInfoDestinationCountryCode, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CarrierInfo', 'getCarrierServiceRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rateParcelsRateParcelBodyParam = {
      fromAddress: {
        company: 'Supplier',
        name: 'J. Smith',
        phone: '303-555-1213',
        email: 'js@example.com',
        residential: false,
        addressLines: [
          '4750 Walnut Street'
        ],
        cityTown: 'Boulder',
        stateProvince: 'CO',
        postalCode: '80301',
        countryCode: 'US'
      },
      toAddress: {
        company: 'Shop',
        name: 'J. Jones',
        phone: '203-000-1234',
        email: 'jjones@example.com',
        residential: false,
        addressLines: [
          '771 Orange St'
        ],
        cityTown: 'New Haven',
        stateProvince: 'CT',
        postalCode: '06511',
        countryCode: 'US'
      },
      parcel: {
        weight: {
          weight: 1,
          unitOfMeasurement: 'OZ'
        },
        dimension: {
          length: 5,
          width: 0.25,
          height: 4,
          unitOfMeasurement: 'IN',
          irregularParcelGirth: 1
        }
      },
      rates: [
        {
          carrier: 'USPS',
          parcelType: 'PKG',
          inductionPostalCode: '06484'
        }
      ]
    };
    describe('#rateParcel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rateParcel(null, null, null, null, null, rateParcelsRateParcelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RateParcels', 'rateParcel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentCreateShipmentLabelBodyParam = {};
    const shipmentxPBTransactionId = 'fakedata';
    describe('#createShipmentLabel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createShipmentLabel(shipmentxPBTransactionId, null, null, null, null, null, shipmentCreateShipmentLabelBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'createShipmentLabel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentOriginalTransactionId = 'fakedata';
    describe('#retryShipment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retryShipment(shipmentOriginalTransactionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'retryShipment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentShipmentId = 'fakedata';
    describe('#reprintShipment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reprintShipment(shipmentShipmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'reprintShipment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const manifestsCreateManifestBodyParam = {};
    describe('#createManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createManifest(shipmentxPBTransactionId, manifestsCreateManifestBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Manifests', 'createManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const manifestsOriginalTransactionId = 'fakedata';
    describe('#retryManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retryManifest(manifestsOriginalTransactionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Manifests', 'retryManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const manifestsManifestId = 'fakedata';
    describe('#reprintManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reprintManifest(manifestsManifestId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Manifests', 'reprintManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const transactionReportsDeveloperId = 'fakedata';
    describe('#getTransactionReport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTransactionReport(transactionReportsDeveloperId, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TransactionReports', 'getTransactionReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pickupGetPickupscheduleBodyParam = {};
    describe('#getPickupschedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPickupschedule(shipmentxPBTransactionId, pickupGetPickupscheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pickup', 'getPickupschedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pickupPickupId = 'fakedata';
    describe('#cancelPickup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelPickup(shipmentxPBTransactionId, pickupPickupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pickup', 'cancelPickup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressValidationVerifyAddressBodyParam = {};
    describe('#verifyAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyAddress(null, addressValidationVerifyAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddressValidation', 'verifyAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressValidationReturnSuggestions = 'fakedata';
    const addressValidationVerifyAndSuggestAddressBodyParam = {};
    describe('#verifyAndSuggestAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyAndSuggestAddress(addressValidationReturnSuggestions, addressValidationVerifyAndSuggestAddressBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AddressValidation', 'verifyAndSuggestAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const parcelProtectionGetParcelProtectionCoverageBodyParam = {
      shipmentInfo: {
        trackingNumber: '940509898641491871138',
        carrier: 'USPS',
        serviceId: 'PM',
        insuranceCoverageValue: 1000,
        insuranceCoverageValueCurrency: 'USD',
        parcelInfo: {
          commodityList: [
            {
              categoryPath: 'electronics',
              itemCode: 'SKU1084',
              name: 'Laptop',
              url: 'https://example.com/computers/laptop/1084'
            }
          ]
        },
        shipperInfo: {
          shipperID: '9024324564',
          address: {
            addressLines: [
              '545 Market St'
            ],
            cityTown: 'San Francisco',
            stateProvince: 'CA',
            postalCode: '94105-2847',
            countryCode: 'US'
          },
          companyName: 'Supplies',
          givenName: 'John',
          middleName: 'James',
          familyName: 'Smith',
          email: 'john@example.com',
          phoneNumbers: [
            {
              number: '1234567890',
              type: 'business phone'
            }
          ]
        },
        consigneeInfo: {
          address: {
            addressLines: [
              '284 W Fulton'
            ],
            cityTown: 'Garden City',
            stateProvince: 'KS',
            postalCode: '67846',
            countryCode: 'US'
          },
          companyName: 'Shop',
          givenName: 'Mary',
          middleName: 'Anne',
          familyName: 'Jones',
          email: 'mary@example.com',
          phoneNumbers: [
            {
              number: '6205551234',
              type: 'business phone'
            },
            {
              number: '6205554321',
              type: 'fax'
            }
          ]
        }
      }
    };
    describe('#getParcelProtectionCoverage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getParcelProtectionCoverage(shipmentxPBTransactionId, parcelProtectionGetParcelProtectionCoverageBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('44397664_ABCD1234_28d6fb80-6b92-4e69-b5bb-6656a00ce435', data.response.transactionID);
                assert.equal('IPPOL0000000012345678', data.response.parcelProtectionReferenceID);
                assert.equal('2019-05-20 15:00:25.279+0000', data.response.parcelProtectionDate);
                assert.equal(7.5, data.response.parcelProtectionFees);
                assert.equal('USD', data.response.parcelProtectionFeesCurrencyCode);
                assert.equal('object', typeof data.response.parcelProtectionFeesBreakup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ParcelProtection', 'getParcelProtectionCoverage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const parcelProtectionGetParcelProtectionQuoteBodyParam = {
      shipmentInfo: {
        carrier: 'USPS',
        serviceId: 'PM',
        insuranceCoverageValue: 1000,
        insuranceCoverageValueCurrency: 'USD',
        parcelInfo: {
          commodityList: [
            {
              categoryPath: 'electronics',
              itemCode: 'SKU1084',
              name: 'Laptop',
              url: 'https://example.com/computers/laptop/1084'
            }
          ]
        },
        shipperInfo: {
          shipperID: '9024324564',
          address: {
            addressLines: [
              '545 Market St'
            ],
            cityTown: 'San Francisco',
            stateProvince: 'CA',
            postalCode: '94105-2847',
            countryCode: 'US'
          }
        },
        consigneeInfo: {
          address: {
            addressLines: [
              '284 W Fulton'
            ],
            cityTown: 'Garden City',
            stateProvince: 'KS',
            postalCode: '67846',
            countryCode: 'US'
          }
        }
      }
    };
    describe('#getParcelProtectionQuote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getParcelProtectionQuote(shipmentxPBTransactionId, parcelProtectionGetParcelProtectionQuoteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7.5, data.response.parcelProtectionFees);
                assert.equal('USD', data.response.parcelProtectionFeesCurrencyCode);
                assert.equal('object', typeof data.response.parcelProtectionFeesBreakup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ParcelProtection', 'getParcelProtectionQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const parcelProtectionParcelProtectionReferenceId = 'fakedata';
    const parcelProtectionCancelParcelProtectionBodyParam = {
      shipperID: '9024324564',
      parcelProtectionAccountID: 'IPACT2345678'
    };
    describe('#cancelParcelProtection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelParcelProtection(shipmentxPBTransactionId, parcelProtectionParcelProtectionReferenceId, parcelProtectionCancelParcelProtectionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Void_Success', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ParcelProtection', 'cancelParcelProtection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const parcelProtectionDeveloperId = 'fakedata';
    const parcelProtectionPolicyCreatedFromDate = 'fakedata';
    describe('#getParcelProtectionReports - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getParcelProtectionReports(shipmentxPBTransactionId, parcelProtectionDeveloperId, parcelProtectionPolicyCreatedFromDate, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ParcelProtection', 'getParcelProtectionReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trackingAddTrackingEventsBodyParam = {
      carrier: 'NEWGISTICS',
      references: [
        {
          referenceType: 'package',
          referenceValue: '1Z00',
          events: [
            {
              eventCode: 'DPB',
              carrierEventCode: 'DOAC',
              eventDate: '2020-04-18',
              eventTime: '12:48:10',
              eventTimeOffset: '-06:00',
              eventCity: 'Decatur',
              eventStateOrProvince: 'IL',
              postalCode: '62521',
              country: 'US'
            }
          ]
        },
        {
          referenceType: 'package',
          referenceValue: '3Z30',
          events: [
            {
              eventCode: 'DPB',
              carrierEventCode: 'DOAC',
              eventDate: '2020-04-18',
              eventTime: '12:50:00',
              eventTimeOffset: '-06:00',
              eventCity: 'Decatur',
              eventStateOrProvince: 'IL',
              postalCode: '62521',
              country: 'US'
            }
          ]
        }
      ]
    };
    describe('#addTrackingEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addTrackingEvents(trackingAddTrackingEventsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tracking', 'addTrackingEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trackingTrackingNumber = 'fakedata';
    const trackingPackageIdentifierType = 'fakedata';
    const trackingCarrier = 'fakedata';
    describe('#getTrackingDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTrackingDetails(trackingTrackingNumber, trackingPackageIdentifierType, trackingCarrier, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tracking', 'getTrackingDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const crossBorderQuotesGetCrossBorderQuotesBodyParam = {
      quoteCurrency: 'USD',
      basketCurrency: 'USD',
      fromAddress: {
        name: 'John Smith',
        residential: false,
        company: 'Supplies',
        addressLines: [
          '545 Market St'
        ],
        cityTown: 'San Francisco',
        stateProvince: 'CA',
        postalCode: '94105',
        countryCode: 'US',
        email: 'john@example.com',
        phone: '415-555-0000'
      },
      toAddress: {
        name: 'Jan Jones',
        residential: true,
        addressLines: [
          '2168 King St N'
        ],
        cityTown: 'Waterloo',
        stateProvince: 'ON',
        postalCode: 'N2J 4G8',
        countryCode: 'CA',
        email: 'jan@example.com',
        phone: '519-555-0000'
      },
      basketItems: [
        {
          brand: '',
          categories: [
            {
              categoryCode: 'UNKNOWN',
              descriptions: [
                {
                  locale: 'en',
                  name: 'Dress',
                  parentsNames: [
                    'Clothing',
                    'Women'
                  ]
                }
              ],
              parentCategoryCode: '6543',
              url: 'www.example.com'
            }
          ],
          description: 'Red Embroidered',
          eccn: 'EAR99',
          hazmats: [
            'hazmat',
            'ormd'
          ],
          hSTariffCode: '4203100001',
          hSTariffCodeCountry: 'AU',
          identifiers: [
            {
              number: '123456',
              source: 'isbn'
            }
          ],
          imageUrls: [
            'www.example.com'
          ],
          itemDimension: {
            length: 11,
            height: 8.5,
            width: 5,
            unitOfMeasurement: 'IN'
          },
          itemId: 'G_123456',
          manufacturer: '',
          originCountryCode: 'CN',
          pricing: {
            price: 20,
            codPrice: [
              {
                price: 20,
                cod: 'CA',
                includesDuty: false,
                includesTaxes: false
              }
            ],
            dutiableValue: 20
          },
          quantity: 2,
          unitPrice: 19.99,
          unitWeight: {
            weight: 5,
            unitOfMeasurement: 'lb'
          },
          url: 'http://www.example.com/products/160921_030'
        }
      ],
      rates: [
        {
          carrier: 'PBI',
          serviceId: 'PBXPS'
        }
      ],
      shipmentOptions: [
        {
          name: 'SHIPPER_ID',
          value: '9024324564'
        },
        {
          name: 'CLIENT_ID',
          value: '789123'
        },
        {
          name: 'CARRIER_FACILITY_ID',
          value: 'US_ELOVATIONS_KY'
        }
      ]
    };
    describe('#getCrossBorderQuotes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCrossBorderQuotes(crossBorderQuotesGetCrossBorderQuotesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.quote));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CrossBorderQuotes', 'getCrossBorderQuotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const crossBorderQuotesPredictedHSCodeBodyParam = {};
    describe('#predictedHSCode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.predictedHSCode(shipmentxPBTransactionId, crossBorderQuotesPredictedHSCodeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CrossBorderQuotes', 'predictedHSCode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const containerGetContainerizedParcelsLabelsBodyParam = {};
    describe('#getContainerizedParcelsLabels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getContainerizedParcelsLabels(shipmentxPBTransactionId, containerGetContainerizedParcelsLabelsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Container', 'getContainerizedParcelsLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelShipment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelShipment(shipmentxPBTransactionId, null, null, null, shipmentShipmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-pitney_bowes-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Shipment', 'cancelShipment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
