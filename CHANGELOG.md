
## 0.4.4 [10-14-2024]

* Changes made at 2024.10.14_19:42PM

See merge request itentialopensource/adapters/adapter-pitney_bowes!16

---

## 0.4.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-pitney_bowes!14

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_17:47PM

See merge request itentialopensource/adapters/adapter-pitney_bowes!13

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_18:43PM

See merge request itentialopensource/adapters/adapter-pitney_bowes!12

---

## 0.4.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!11

---

## 0.3.7 [03-26-2024]

* Changes made at 2024.03.26_14:39PM

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!10

---

## 0.3.6 [03-21-2024]

* Changes made at 2024.03.21_14:40PM

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!9

---

## 0.3.5 [03-13-2024]

* Changes made at 2024.03.13_14:00PM

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!8

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.11_14:09PM

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!7

---

## 0.3.3 [02-26-2024]

* Changes made at 2024.02.26_13:35PM

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!6

---

## 0.3.2 [12-24-2023]

* update metadata

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!5

---

## 0.3.1 [12-20-2023]

* Patch/axios

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!4

---

## 0.3.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!2

---

## 0.2.0 [11-07-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-pitney_bowes!2

---

## 0.1.2 [02-28-2023]

* Bug fixes and performance improvements

See commit 4384641

---

## 0.1.1 [02-17-2023]

* Bug fixes and performance improvements

See commit e9f3b89

---
