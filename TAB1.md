# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Pitney_bowes System. The API that was used to build the adapter for Pitney_bowes is usually available in the report directory of this adapter. The adapter utilizes the Pitney_bowes API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Pitney Bowes adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Pitney Bowes. With this adapter you have the ability to perform operations such as:

- Create, Reprint, Retry, and Void a Shipment.
- Create, Reprint and Retry Manifest.
- Validate and Suggest Addresses.
- Get transaction or archived reports.
- Tracking

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
